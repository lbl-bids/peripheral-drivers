`timescale 1ns / 1ns

module spi_ad9268_tb;

reg clk=0;


// spi_master wires
reg spi_rw;
reg [8:0] spi_addr;
reg [7:0] spi_data;
wire spi_start;
wire cs, sck, sdo;
wire [6:0] sdo_addr;
wire [7:0] spi_rdbk;
wire [7:0] read_r;

initial begin
    if($test$plusargs("vcd")) begin
        $dumpfile("spi_ad9268.vcd");
        $dumpvars(5,spi_ad9268_tb);
    end

    spi_rw=0;
    spi_addr=9'h04;
    spi_data=8'h21;
    while ($time<10000) begin
        #10;
    end
    $finish;
end
always #5 clk=~clk; // 100 MHz clk


reg start=0;
//============================================================
// spi start generater
//============================================================
always @(posedge clk) begin
    if (($time>50)&&($time<100))
        start<=1;
    else
        start<=0;
end



spi_ad9268 #(.tsckw(3)) spi_master(
	.clk(clk), .strobe(spi_start), .ce(1'b1), .rd(spi_rw),
	.ack(), .addr(spi_addr), .din(spi_data), .read_r(read_r)
);
//============================================================
// strobe_gen instantiation
//============================================================
strobe_gen strobe_gen(
    .I_clk(clk), .I_signal(start), .O_strobe(spi_start)
);

endmodule
