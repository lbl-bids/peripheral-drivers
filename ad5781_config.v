`timescale 1ns / 1ps

module ad5781_config(input clk
,input enable
,input load
,output spi_start
,output [ADDR_WIDTH-1:0] spi_addr
,output [DATA_WIDTH-1:0] spi_data
,input [0:0] busy
,input signed [17:0] din
,input [3:0] dac_command
);
function integer clog2;
	input integer value;
	begin
		value = value-1;
		for (clog2=0; value>0; clog2=clog2+1)
			value = value>>1;
	end
endfunction
parameter ADDR_WIDTH=4;
parameter DATA_WIDTH=20;
parameter INITLENGTH=4;
localparam INITWIDTH=clog2(INITLENGTH+1)+1;
reg [ADDR_WIDTH+DATA_WIDTH-1:0] init [INITLENGTH-1:0];
initial begin
init[0]= {4'b0001,20'd0};  // reset DAC register to 0
init[1]= {4'b0011,20'd0}; // reset clear code register to 0
init[2]= {4'b0010,12'd0,4'b0010,4'b0010};  // reset 
init[3]= {4'b0000,16'd0,4'b0000};  // No Operation
end
reg enable_d = 1'b0;
reg enable_d2 = 1'b0;
wire reset = enable_d & ~enable_d2;
reg reseting=0;
reg [INITWIDTH-1:0] initaddr=0;
wire resetdone=load_d&(initaddr==INITLENGTH);

reg [ADDR_WIDTH-1:0] spi_addr_r=0;
reg [DATA_WIDTH-1:0] spi_data_r=0;
reg load_d=0;
reg load_d2=0;
reg load_d3=0;
wire [23:0] initi=init[initaddr];
always @(posedge clk) begin
	enable_d <= enable;
	enable_d2 <= enable_d;
	load_d<=~busy &load;
	load_d2<=load_d;
	load_d3<=load_d2;

	reseting <= enable_d ? ~enable_d2 ? 1'b1 : resetdone ? 1'b0 : reseting : 1'b0;
	initaddr<=reset ? 0 : reseting & load_d2 ? initaddr+1 : initaddr;
	if (load_d2) begin
		{spi_addr_r,spi_data_r} <= reseting ? init[initaddr]: {dac_command,din,2'b0};
	end
end
assign spi_start=enable_d ? load_d2:0;
assign spi_addr=enable_d ? spi_addr_r:0;
assign spi_data=enable_d ? spi_data_r:0;
endmodule
