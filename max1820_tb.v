`timescale 1ns / 1ns

module max1820_tb();

wire chip_sclk, chip_sdio, chip_csb;
reg dspclk, usbclk;
reg sync_enable=0;
reg mux_ctl=0;
reg [2:0] div_ctl=7;

wire sync; // output

max1820 dut(.dspclk(dspclk), .usbclk(usbclk), .sync(sync),
	.sync_enable(sync_enable), .mux_ctl(mux_ctl),
	.div_ctl(div_ctl)
);

integer cc;
initial begin
	if ($test$plusargs("vcd")) begin
		$dumpfile("max1820.vcd");
		$dumpvars(5,max1820_tb);
	end
	usbclk=0;
	dspclk=0;
	for (cc=0; cc<100; cc=cc+1) begin
		#11; usbclk=1;
		#11; usbclk=0;
	end
	$finish();
end

always begin
	#6; dspclk=~dspclk;
end

always @(posedge usbclk) begin
	if (cc == 5) sync_enable <= 1;
	if (cc == 50) begin
		mux_ctl <= 1;
	end
end

endmodule
