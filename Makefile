.PHONY: all clean

UNISIM_PATH = $(XILINX)/verilog/src/unisims

ICARUS_SUFFIX=
VERILOG = iverilog$(ICARUS_SUFFIX) -Wall -Wno-timescale
VVP = vvp$(ICARUS_SUFFIX) -n
GTKWAVE = gtkwave
AWK = awk

# broken: mcp3208_wrap_tb
TGT = ad56x4_tb ad95xx_tb ds1822_tb max1820_tb mcp3208_tb sporta_tb ltc2174_tb ad9653_tb ad9781_tb spi_master_tb spi_ad9268_tb

%_tb: %_tb.v
	$(VERILOG) -y. -o $@ $^

%_check: %_tb testcode.awk
	$(VVP) $< | $(AWK) -f $(filter %.awk, $^)

%.vcd: %_tb
	$(VVP) $< +vcd

%_view: %.vcd %.sav
	$(GTKWAVE) $^

all: $(TGT)

ltc2174_tb: ltc2174_tb.v ltc2174.v
	$(VERILOG) -y. -y$(UNISIM_PATH) -o $@ $^

# missing regression tests: ltc2174 max1820 spi_ad9268 spi_master sporta
checks: ad56x4_check ad95xx_check ds1822_check mcp3208_check

mcp3208_check: mcp3208_tb mcp3208_check.awk
	$(VVP) $< | $(AWK) -f $(filter %.awk, $^)

ds1822_tb: ds1822_driver.v ds1822.v ds1822_state.v

clean:
	rm -f $(TGT) *.vcd
