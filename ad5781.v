`timescale 1ns / 1ps
module ad5781(
input clk
,output SCLK
,output SDIO
,output SYNC
,input [ADDR_WIDTH-1:0] spi_addr
,input [DATA_WIDTH-1:0] spi_data
,input  spi_start
,output  sdio_as_sdo
,input  sdo
,output [ADDR_WIDTH-1:0] sdo_addr
,output [DATA_WIDTH-1:0] spi_rdbk
,input  spi_read
,output  spi_ready
,output busy
);
parameter TSCKHALF=9;
parameter ADDR_WIDTH=4;
parameter DATA_WIDTH=20;
spi_master #(.TSCKHALF(TSCKHALF),.ADDR_WIDTH(ADDR_WIDTH),.DATA_WIDTH(DATA_WIDTH))
spi_master(.cs(SYNC),.sck(SCLK),.sdi(SDIO)
,.clk(clk)
,.spi_start(spi_start)
,.spi_addr(spi_addr)
,.spi_data(spi_data)
,.sdio_as_sdo(sdio_as_sdo)
,.sdo(sdo)
,.sdo_addr(sdo_addr)
,.spi_rdbk(spi_rdbk)
,.spi_read(spi_read)
,.spi_ready(spi_ready)
);
assign busy=~SYNC;
endmodule
