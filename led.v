module led (LED
,led);
output LED;
input [0:0] led;
assign LED=led;
endmodule
