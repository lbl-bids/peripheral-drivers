`timescale 1ns / 1ps
module ad5781_sim(input SCLK
,input SDIO
,input SYNC
,input clk
,output [ADDR_WIDTH-1:0] spi_addr
,output [DATA_WIDTH-1:0] spi_data
);
parameter TSCKHALF=9;
parameter ADDR_WIDTH=4;
parameter DATA_WIDTH=20;
spi_slave#(.ADDR_WIDTH(ADDR_WIDTH),.DATA_WIDTH(DATA_WIDTH))
spi_slave(.CSB(SYNC),.SCK(SCLK),.SDIO(SDIO)
,.clk(clk)
,.addr(spi_addr)
,.data(spi_data)
);
assign busy=~SYNC;
endmodule
